# Base image
FROM ubuntu:latest

# Set working directory
WORKDIR /app

# Install dependencies
RUN apt-get update && apt-get install -y gcc make

# Copy source code
COPY . /app

# Build the server and client programs
RUN gcc -o database program_database.c
RUN gcc -o client program_client.c

# Expose the necessary port for the server
EXPOSE 8080

# Run the database server and client
CMD ./database & sleep 5 && ./client

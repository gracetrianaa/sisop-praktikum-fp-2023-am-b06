# sisop-praktikum-fp-2023-AM-B06

Anggota kelompok B06 adalah :

|        Nama           | NRP|
| ---                   |--- |
|Glenaya                |5025211202 |
|Gracetriana Survinta  Septinaputri | 5025211199 |
|Ken Anargya Alkausar   | 5025211168 |

# Laporan Resmi Final Praktikum 
## Pembahasan File client.c

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <dirent.h>
#include <ctype.h>
#include <time.h>

#define PORT 4443

struct user_allowed {
    char name[10000];
    char password[10000];
};

```
Dalam kode di atas terdapat library yang digunakan dalam file ini. Lalu ```PORT``` didefinisikan dengan konstanta 4443 sebagai nomor port yang akan digunakan dalam komunikasi melalui socket. Selain itu, terdapat struct ```user_allowed``` yang memiliki dua anggota, yaitu ```name``` dan ```password```. Struct ini digunakan untuk menyimpan informasi nama pengguna dan kata sandi.


```c
int checkAllowed(char *username, char *password) {
    FILE *fp;
    struct user_allowed user;
    int found = 0;
    fp = fopen("home/grace/Documents/database/user.txt", "rb");
    if (fp == NULL) {
        printf("Cannot open user file.\n");
        return 0;
    }
    while (fread(&user, sizeof(user), 1, fp)) {
        if (strcmp(user.name, username) == 0) {
            if (strcmp(user.password, password) == 0) {
                found = 1;
                break;
            }
        }
    }
    fclose(fp);
    if (found == 0) {
        printf("No Permission\n");
        return 0;
    } else {
        return 1;
    }
}
```
Kode di atas merupakan fungsi yang bernama ```checkAllowed``` yang berguna untuk memeriksa apakah suatu pengguna (berdasarkan username dan password) diizinkan atau memiliki izin akses tertentu. Kode ini akan mengecek kecocokan antara input username dan password dengan yang ada di ```user.txt```. Penjelasan kode tersebut adalah sebagai berikut :

- Fungsi ```checkAllowed``` mengambil dua parameter, yaitu ```username``` dan ```password```, yang merupakan string yang mewakili username dan password pengguna yang ingin diverifikasi.

- Pertama-tama, fungsi ini membuka file dengan nama ```"home/grace/Documents/database/user.txt"``` dalam mode baca biner ("rb") menggunakan fungsi ```fopen()```. Jika file tidak dapat dibuka (fp == NULL), maka pesan kesalahan ```Cannot open user file``` akan dicetak dan fungsi mengembalikan nilai 0, menandakan bahwa pengguna tidak diizinkan.

- Selanjutnya, fungsi ini melakukan loop dengan menggunakan fungsi ```fread()``` untuk membaca struct ```user_allowed``` dari file secara berulang. Setiap kali membaca struct, fungsi membandingkan nilai name dalam struct dengan username yang diberikan menggunakan fungsi ```strcmp()```. Jika kedua string tersebut sama, maka fungsi membandingkan juga nilai password dalam struktur dengan password yang diberikan. Jika kedua string tersebut juga sama, variabel found diatur menjadi 1 dan loop dihentikan dengan menggunakan break.

- Setelah selesai membaca file, fungsi menutup file menggunakan ```fclose(fp)```.

- Terakhir, jika variabel found masih bernilai 0, artinya tidak ada kecocokan username dan password yang ditemukan dalam file, maka pesan ```No Permission``` akan dicetak dan fungsi mengembalikan nilai 0. Jika found bernilai 1, artinya ada kecocokan dan izin ditemukan, fungsi mengembalikan nilai 1, menandakan bahwa pengguna diizinkan.

```c
int main(int argc, char *argv[]) {
    int user_allowed = 0;
    int id_user = geteuid();
    char database_used[1000];
    if (id_user == 0) {
        user_allowed = 1;
    } else {
        user_allowed = checkAllowed(argv[2], argv[4]);
    }
    if (user_allowed == 0) {
        return 0;
    }
    int clientSocket, ret;
    struct sockaddr_in serverAddr;
    char buffer[32000];

    clientSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (clientSocket < 0) {
        printf("[-]Error in connection.\n");
        exit(1);
    }
    printf("[+]Client Socket is created.\n");

    memset(&serverAddr, '\0', sizeof(serverAddr));
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(PORT);
    serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
```
Selanjutnya terdapat juga fungsi ```main()``` yang memiliki dua parameter. Terdapat tiga variabel yang dideklarasi di fungsi ini yaitu :
- ```user_allowed``` diinisialisasi dengan nilai 0. Variabel ini berguna untuk menyimpan status izin akses pengguna
- ```id_user``` yang memanggil fungsi ```geteuid()``` untuk mendapatkan ID pengguna saat ini
- ```database_used``` untuk menyimpan nama database yang digunakan.

Setelah itu, dilakukan pengecekan terhadap ```id_user```. Jika ```id_user``` sama dengan 0, artinya pengguna menjalankan program dengan hak akses root (superuser). Dalam hal ini, variabel user_allowed diatur menjadi 1, menunjukkan bahwa pengguna diizinkan.

Jika ```id_user``` tidak sama dengan 0, artinya pengguna menjalankan program sebagai pengguna biasa. Dalam hal ini, dilakukan pemanggilan fungsi ```checkAllowed(argv[2]```, ```argv[4])``` dengan argumen ```argv[2]``` sebagai username dan ```argv[4]``` sebagai password untuk memeriksa izin akses pengguna. Hasil dari pemanggilan ini disimpan dalam variabel ```user_allowed```.

Setelah pengecekan izin akses, dilakukan pengecekan nilai ```user_allowed```. Jika nilainya 0, artinya pengguna tidak diizinkan atau tidak melewati autentikasi. Dalam hal ini, program langsung keluar dengan mengembalikan nilai 0 (return 0).

Jika nilai ```user_allowed``` tidak sama dengan 0, artinya pengguna diizinkan, maka program melanjutkan dengan membuat soket klien ```(clientSocket)``` menggunakan fungsi ```socket()```. Jika pembuatan soket gagal, pesan kesalahan akan dicetak, dan program keluar dengan menggunakan ```exit(1)```.

Selanjutnya, dilakukan pengaturan informasi server pada variabel ```serverAddr``` dengan menggunakan fungsi ```memset()``` untuk menginisialisasi struktur ```serverAddr``` menjadi nol, kemudian mengatur atribut-atributnya.

- ```serverAddr.sin_family ``` diatur sebagai ```AF_INET``` yang menunjukkan penggunaan protokol ```IPv4```.

- ```serverAddr.sin_port ```diatur sebagai ```htons(PORT)``` yang merupakan nomor port yang telah ditentukan sebelumnya (4443).

- ```serverAddr.sin_addr.s_addr``` diatur sebagai ```inet_addr("127.0.0.1")``` yang merupakan alamat IP lokal loopback (localhost).

```c
    ret = connect(clientSocket, (struct sockaddr*)&serverAddr, sizeof(serverAddr));
    if (ret < 0) {
        printf("[-]Error in connection.\n");
        exit(1);
    }
    printf("[+]Connected to Server.\n");

```

Kode ini merupakan lanjutan dari isi fungsi ```main()``` yang berfungsi untuk pemanggilan fungsi ```connect()``` untuk melakukan konseksi antara soket klien ```(cilentSocket)``` dengan alamat server yang ditentukan ```(serverAddr)``` 

```c
while (1) {
        printf("Client: \t");
        char input[1000];
        char copyinput[1000];
        char command[100][1000];
        char *token;
        int i = 0;
        scanf(" %[^\n]s", input);
        strcpy(copyinput, input);
        token = strtok(input, " ");
        while (token != NULL) {
            strcpy(command[i], token);
            i++;
            token = strtok(NULL, " ");
        }
        int wrongCommand = 0;
        if (strcmp(command[0], "CREATE") == 0) {
            if (strcmp(command[1], "USER") == 0 && strcmp(command[3], "IDENTIFIED") == 0 &&
                strcmp(command[4], "BY") == 0) {
                snprintf(buffer, sizeof(buffer), "cUser:%s:%s:%d", command[2], command[5], id_user);
                send(clientSocket, buffer, strlen(buffer), 0);
            } else if (strcmp(command[1], "DATABASE") == 0) {
                snprintf(buffer, sizeof(buffer), "cDatabase:%s:%s:%d", command[2], argv[2], id_user);
                send(clientSocket, buffer, strlen(buffer), 0);
            } else if (strcmp(command[1], "TABLE") == 0) {
                snprintf(buffer, sizeof(buffer), "cTable:%s", copyinput);
                send(clientSocket, buffer, strlen(buffer), 0);
            }
        } else if (strcmp(command[0], "GRANT") == 0 && strcmp(command[1], "PERMISSION") == 0 &&
                   strcmp(command[3], "INTO") == 0) {
            snprintf(buffer, sizeof(buffer), "iPermission:%s:%s:%d", command[2], command[4], id_user);
            send(clientSocket, buffer, strlen(buffer), 0);
        } else if (strcmp(command[0], "USE") == 0) {
            snprintf(buffer, sizeof(buffer), "uDatabase:%s:%s:%d", command[1], argv[2], id_user);
            send(clientSocket, buffer, strlen(buffer), 0);
        } else if (strcmp(command[0], "cekCurrentDatabase") == 0) {
            snprintf(buffer, sizeof(buffer), "%s", command[0]);
            send(clientSocket, buffer, strlen(buffer), 0);

        } else if (strcmp(command[0], "INSERT") == 0 && strcmp(command[1], "INTO") == 0) {
            snprintf(buffer, sizeof(buffer), "insert:%s", copyinput);
            send(clientSocket, buffer, strlen(buffer), 0);
        } else if (strcmp(command[0], "UPDATE") == 0) {
            snprintf(buffer, sizeof(buffer), "update:%s", copyinput);
            send(clientSocket, buffer, strlen(buffer), 0);
        } else if (strcmp(command[0], "DELETE") == 0) {
            snprintf(buffer, sizeof(buffer), "delete:%s", copyinput);
            send(clientSocket, buffer, strlen(buffer), 0);
        } else if (strcmp(command[0], "SELECT") == 0) {
            snprintf(buffer, sizeof(buffer), "select:%s", copyinput);
            send(clientSocket, buffer, strlen(buffer), 0);
        } else if (strcmp(command[0], "DROP") == 0) {
            if (strcmp(command[1], "DATABASE") == 0) {
                snprintf(buffer, sizeof(buffer), "dDatabase:%s:%s", command[2], argv[2]);
                send(clientSocket, buffer, strlen(buffer), 0);
            } else if (strcmp(command[1], "TABLE") == 0) {
                snprintf(buffer, sizeof(buffer), "dTable:%s:%s", command[2], argv[2]);
                send(clientSocket, buffer, strlen(buffer), 0);
            } else if (strcmp(command[1], "COLUMN") == 0) {
                snprintf(buffer, sizeof(buffer), "dColumn:%s:%s:%s", command[2], command[4], argv[2]);
                send(clientSocket, buffer, strlen(buffer), 0);
            }
        } 
     

        if (strcmp(command[0], ":exit") == 0) {
            send(clientSocket, command[0], strlen(command[0]), 0);
            close(clientSocket);
            printf("[-]Disconnected from server.\n");
            exit(1);
        }
        bzero(buffer, sizeof(buffer));
        if (recv(clientSocket, buffer, 1024, 0) < 0) {
            printf("[-]Error in receiving data.\n");
        } else {
            printf("Server: \t%s\n", buffer);
        }
    }

    return 0;
}
```
Kode di atas juga merupakan potongan kode lanjutan dari kode sebelumnya. Kode ini merupakan bagian dari loop utama dalam fungsi ```main()```.

- Loop ```while (1)``` menandakan bahwa bagian di dalam loop akan terus dijalankan selama kondisi tersebut bernilai true. Dalam hal ini, karena kondisinya selalu 1 (atau true), loop akan berjalan tanpa henti.

- Di dalam loop, pertama-tama program mencetak prompt ```Client: \t``` untuk menunjukkan bahwa pengguna dapat memasukkan perintah dari sisi klien.

- Kemudian, variabel-variabel yang digunakan untuk memproses perintah diinisialisasi. ```char input[1000]``` digunakan untuk menyimpan input perintah dari pengguna, ```char copyinput[1000]``` digunakan untuk membuat salinan dari input, ```char command[100][1000]``` digunakan untuk menyimpan token-token perintah yang telah dipisahkan, dan ```char *token``` digunakan sebagai penanda token saat melakukan pemisahan kata-kata dalam input.

- Pemanggilan ```scanf(" %[^\n]s", input)``` digunakan untuk membaca input perintah dari pengguna.

- Setelah membaca input, program membuat salinan dari input menggunakan ```strcpy(copyinput, input)```. 

- Selanjutnya, perintah dipecah menjadi token-token menggunakan ```strtok()```. ```strtok()``` berfungsi untuk membagi sebuah string menjadi token-token yang dipisahkan oleh delimiter tertentu. Di sini, delimiter yang digunakan adalah spasi (" ").

- Loop ```while (token != NULL)``` digunakan untuk mengambil token-token perintah yang telah dipisahkan. Setiap token disalin ke dalam array ```command``` pada indeks yang sesuai, dan indeks ```i``` diperbarui untuk mengarah ke indeks berikutnya. Dalam loop ini, perintah-perintah yang telah dipisahkan akan disimpan dalam array ```command``` untuk digunakan nanti.

- Setelah pemisahan perintah selesai, program akan memeriksa perintah yang diberikan oleh pengguna dan mengirimkannya ke server menggunakan soket klien. Beberapa perintah diperiksa menggunakan ```if-else``` untuk melakukan tindakan yang sesuai. Misalnya, jika perintah adalah "CREATE USER", "CREATE DATABASE", "CREATE TABLE", "GRANT PERMISSION", "USE", "INSERT INTO", "UPDATE", "DELETE", atau "SELECT", maka program akan membentuk pesan yang sesuai menggunakan ```snprintf()``` dan mengirimkannya ke server menggunakan ```send()```. Jika perintah adalah "DROP DATABASE", "DROP TABLE", atau "DROP COLUMN", program juga akan membentuk pesan yang sesuai dan mengirimkannya ke server.

- Jika perintah adalah ":exit", program akan mengirim pesan ":exit" ke server, menutup soket klien, lalu mencetak pesan "[-]Disconnected from server" lalu keluar dari program dengan ```exit(1)```

- Setelah mengirim perintah ke server, program akan mengosongkan ```buffer``` menggunakan ```bzero(buffer, sizeof(buffer))``` untuk memastikan bahwa tidak ada data yang tersisa sebelum menerima respons dari server.

- Selanjutnya, program menerima respons dari server menggunakan ```recv()```. Jika ada kesalahan dalam menerima data, program mencetak pesan ```"[-]Error in receiving data."```. Jika tidak ada kesalahan, respons dari server akan disimpan dalam buffer dan kemudian dicetak ke layar dengan pesan ```"Server: \t%s\n"```.

- Loop akan berlanjut dari awal, meminta pengguna untuk memasukkan perintah baru, mengirimkannya ke server, dan menerima respons hingga perintah keluar dari program ```("exit")``` diberikan.

- Setelah loop selesai, program akan mengembalikan nilai ```0```, menandakan bahwa eksekusi program telah selesai.

## Program Database

Program `database.c` ini berfungsi sebagai server dari database yang akan menangkap dan menjalankan command-command dari client.

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>

#define PORT 4443

struct allowed{
	char name[10000];
	char password[10000];
};

struct used_database{
	char database[10000];
	char name[10000];
};

struct table{
	int jumlahkolom;
	char type[100][10000];
	char data[100][10000];
};
```
Program diawali dengan mendefinisikan library yang akan digunakan, port dan membuat `struct` untuk user `allowed`, database yang akan digunakan `used_database` dan `table` untuk tabel. 

```c
void createUser(const char *nama, const char *password){
	struct allowed user;
	strcpy(user.name, nama);
	strcpy(user.password, password);
	printf("%s %s\n", user.name, user.password);
	char fname[]={"home/grace/Documents/database/user.txt"};
	FILE *fp;
	fp=fopen(fname,"ab");
	fwrite(&user,sizeof(user),1,fp);
	fclose(fp);
}
```
Fungsi `createUser` memiliki fungsi untuk membuat dan menyimpan user (nama dan password) yang akan mengakses database. Nama dan password disimpan ke dalam struct menggunakan `strcpy` kemudian ditambahkan ke dalam file `user.txt` yang berada di dalam path `fname`.

```c

int CheckUserExist(const char *username){
	FILE *fp;
	struct allowed user;
	int found=0;
	fp=fopen("home/grace/Documents/database/user.txt","rb");
	while(fread(&user,sizeof(user),1,fp) != 0){	
		if(strcmp(user.name, username)==0){
			fclose(fp);
			return 1;
		}
	}
	fclose(fp);
	return 0;
}
```
Fungsi `CheckUserExist` digunakan untuk melakukan pengecekan apakah user sudah ada atau belum. DI dalam fungsi tersebut akan dibuka file `user.txt` yang menyimpan data user kemudian jika username ditemukan maka akan dikembalikan nilai 1.

```c
void forPermission(const char *nama, const char *database){
	struct used_database user;
	strcpy(user.name, nama);
	strcpy(user.database, database);
	printf("%s %s\n", user.name, user.database);
	char fname[]={"database/permission.txt"};
	FILE *fp;
	fp=fopen(fname,"ab");
	fwrite(&user,sizeof(user),1,fp);
	fclose(fp);
}
```
Fungsi `forPermission` akan menyimpan permission dari tiap user. Nama dari tiap user dan database yang diakses akan disimpan ke struct `used_database` menggunakan `strcpy` yang kemudian akan ditulis ke file `permission.txt` yang berada di path sesuai yang dinyatakan string `fname`.

```c

int checkAllowedDatabase(const char *nama, const char *database){
	FILE *fp;
	struct used_database user;
	fp=fopen("home/grace/Documents/database/permission.txt","rb");
	while(fread(&user,sizeof(user),1,fp) != 0){	
		if(strcmp(user.name, nama)==0 && strcmp(user.database, database)==0){
			fclose(fp);
			return 1;
		}
	}
	fclose(fp);
	return 0;
}
```
Fungsi `checkAllowedDatabase` akan melakukan pengecekan database yang dipakai oleh user. File `permission.txt` akan dibaca dan dilakukan pengecekan apakah ditemukan nama user dan database yang dipakai, jika ada maka akan mengembalikan nilai 1.

```c

void writelog(const char *command, const char *nama){
	time_t rawtime;
	struct tm *timeinfo;
	time(&rawtime);
	timeinfo = localtime(&rawtime);
 
	char infoWriteLog[1000];

	FILE *file;
	file = fopen("logUser.log", "ab");

	sprintf(infoWriteLog, "%d-%.2d-%.2d %.2d:%.2d:%.2d::%s::%s\n",timeinfo->tm_year + 1900, timeinfo->tm_mon + 1, timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, nama, command);

	fputs(infoWriteLog, file);
	fclose(file);
	return;
}
```
Fungsi `writelog` akan menyimpan histori log yang dilakukan oleh user. File logUser.log dibuat dan dibuka untuk menyimpan data tahun, bulan, hari, jam, detik, nama user, dan command yang dilakukan menggunakan `sprintf`.

```c
int findCol(const char *table, const char *kolom){
	FILE *fp;
	struct table user;
	fp=fopen(table,"rb");	
	fread(&user,sizeof(user),1,fp);
	int index=-1;
	for(int i=0; i < user.jumlahkolom; i++){
		if(strcmp(user.data[i], kolom)==0){
			index = i;
		}
	}
	fclose(fp);
	return index;
}
```
Fungsi `findCol` bertujuan untuk mencari indeks kolom dalam sebuah tabel. Pertama, kode membuka file dengan mode baca biner menggunakan `fopen`. Kemudian, menggunakan `fread`, kode membaca satu struct user dari file yang berisi data tabel. Selanjutnya, dilakukan perulangan untuk memeriksa setiap kolom dalam tabel dengan membandingkan nilai kolom pada indeks saat ini dengan nilai yang dicari menggunakan `strcmp`. Jika kolom ditemukan, maka indeks kolom tersebut disimpan dalam variabel index. Setelah selesai perulangan, file ditutup dengan `fclose` dan nilai index dikembalikan sebagai hasil fungsi. Jika kolom tidak ditemukan, fungsi akan mengembalikan nilai -1. Dengan demikian, fungsi findCol dapat digunakan untuk mencari indeks kolom dalam sebuah tabel berdasarkan nama kolom yang diberikan.

```c
int delCol(const char *table, int index){
	FILE *fp, *fp1;
	struct table user;
	fp=fopen(table,"rb");
	fp1=fopen("temp","wb");
	while(fread(&user,sizeof(user),1,fp) != 0){	
		struct table userCopy;
		int iterasi=0;
		for(int i=0; i< user.jumlahkolom; i++){
			if(i == index){
				continue;
			}
			strcpy(userCopy.data[iterasi], user.data[i]);
			strcpy(userCopy.type[iterasi], user.type[i]);
			iterasi++;
		}
		userCopy.jumlahkolom = user.jumlahkolom-1;
		fwrite(&userCopy,sizeof(userCopy),1,fp1);
	}
	fclose(fp);
	fclose(fp1);
	remove(table);
	rename("temp", table);
	return 0;
}
```
Fungsi `delCol` bertujuan untuk menghapus kolom dalam sebuah tabel. Pertama, kode membuka file table dan membuka file temp menggunakan `fopen`. Selanjutnya, menggunakan `fread`, kode membaca satu struktur user dari file yang berisi data tabel. Dilakukan perulangan untuk setiap user yang dibaca, dan pada setiap iterasi, sebuah struct `userCopy` dibuat sebagai salinan dari user. Dalam perulangan tersebut, jika indeks saat ini sama dengan indeks kolom yang ingin dihapus, maka kolom tersebut akan dilewati dengan perintah continue. Jika indeks saat ini tidak sama dengan indeks kolom yang ingin dihapus, nilai kolom dan tipe kolom akan disalin ke `userCopy`. Setelah selesai perulangan, jumlah kolom dalam userCopy diubah menjadi jumlah kolom sebelumnya dikurangi satu. Selanjutnya, menggunakan `fwrite`, userCopy ditulis ke file `temp`. Setelah selesai perulangan, kedua file ditutup menggunakan `fclose`, file asli dihapus menggunakan `remove`, dan file temp diubah namanya menjadi nama file asli menggunakan `rename`. Terakhir, fungsi mengembalikan nilai 0. 

```c
int updateCol(const char *table, int index, const char *ganti){
    FILE *fp, *fp1;
	struct table user;
	fp=fopen(table,"rb");
    fp1=fopen("temp","ab");
    int datake = 0;
	while(fread(&user,sizeof(user),1,fp) != 0){	
        struct table userCopy;
        int iterasi=0;
        for(int i=0; i< user.jumlahkolom; i++){
            if(i == index && datake!=0){
                strcpy(userCopy.data[iterasi], ganti);
            }else{
                strcpy(userCopy.data[iterasi], user.data[i]);
            }
            strcpy(userCopy.type[iterasi], user.type[i]);
            iterasi++;
        }
        userCopy.jumlahkolom = user.jumlahkolom;
        fwrite(&userCopy,sizeof(userCopy),1,fp1);
        datake++;
	}
	fclose(fp);
	fclose(fp1);
	remove(table);
	rename("temp", table);
	return 0;
}

int updateColumnW(const char *table, int index, const char *ganti, int indexGanti, const char *where){
    FILE *fp, *fp1;
	struct table user;
	fp=fopen(table,"rb");
    fp1=fopen("temp","ab");
    int datake = 0;
	while(fread(&user,sizeof(user),1,fp) != 0){	
        struct table userCopy;
        int iterasi=0;
        for(int i=0; i< user.jumlahkolom; i++){
            if(i == index && datake!=0 && strcmp(user.data[indexGanti], where)==0){
                strcpy(userCopy.data[iterasi], ganti);
            }else{
                strcpy(userCopy.data[iterasi], user.data[i]);
            }
            strcpy(userCopy.type[iterasi], user.type[i]);
            iterasi++;
        }
        userCopy.jumlahkolom = user.jumlahkolom;
        fwrite(&userCopy,sizeof(userCopy),1,fp1);
        datake++;
	}
	fclose(fp);
	fclose(fp1);
	remove(table);
	rename("temp", table);
	return 0;
}
```
Fungsi `updateCol` merupakan implementasi dari fungsi yang bertujuan untuk mengupdate nilai kolom dalam sebuah tabel. Pertama, kode membuka file dengan mode baca biner menggunakan fopen dan membuka file temp dengan mode tambah biner menggunakan fopen juga. Selanjutnya, menggunakan fread, kode membaca satu struktur user dari file yang berisi data tabel. Dilakukan perulangan untuk setiap user yang dibaca, dan pada setiap iterasi, sebuah struktur userCopy dibuat sebagai salinan dari user. Dalam perulangan tersebut, jika indeks saat ini sama dengan indeks kolom yang akan diupdate dan bukan iterasi pertama (datake!=0), nilai kolom akan diganti dengan nilai baru (ganti). Jika bukan kasus tersebut, nilai kolom tetap sama dengan nilai sebelumnya. Setelah selesai perulangan, jumlah kolom dalam userCopy diubah menjadi jumlah kolom sebelumnya, dan userCopy ditulis ke file temp menggunakan fwrite. Setelah itu, kedua file ditutup, file asli dihapus menggunakan remove, dan file temp diubah namanya menjadi nama file asli menggunakan rename. Fungsi updateCol mengembalikan nilai 0.

Fungsi `updateColumnW` mirip dengan yang pertama, namun memiliki parameter tambahan yaitu indexGanti dan where. indexGanti adalah indeks kolom yang akan digunakan untuk memeriksa kondisi (where), jika kondisi terpenuhi, maka nilai kolom akan diupdate. Jadi, updateColumnW digunakan untuk mengupdate nilai kolom berdasarkan kondisi tertentu dalam sebuah tabel.

```c
int deleteTable(const char *table, const char *namaTable){
    FILE *fp, *fp1;
	struct table user;
	fp=fopen(table,"rb");
    fp1=fopen("temp","ab");	
	fread(&user,sizeof(user),1,fp);
    struct table userCopy;
	for(int i=0; i < user.jumlahkolom; i++){
        strcpy(userCopy.data[i], user.data[i]);
        strcpy(userCopy.type[i], user.type[i]);
	}
    userCopy.jumlahkolom = user.jumlahkolom;
    fwrite(&userCopy,sizeof(userCopy),1,fp1);
    fclose(fp);
	fclose(fp1);
    remove(table);
	rename("temp", table);
	return 1;
}

int deleteTableW(const char *table, int index, const char *kolom, const char *where){
    FILE *fp, *fp1;
	struct table user;
	fp=fopen(table,"rb");
    fp1=fopen("temp","ab");
    int datake = 0;
	while(1){	
        int found = 0;
		fread(&user,sizeof(user),1,fp);
        if(feof(fp)){
			break;
		}
        struct table userCopy;
        int iterasi=0;
        for(int i=0; i< user.jumlahkolom; i++){
            if(i == index && datake!=0 && strcmp(user.data[i], where)==0){
                found = 1;
            }
            strcpy(userCopy.data[iterasi], user.data[i]);
            strcpy(userCopy.type[iterasi], user.type[i]);
            iterasi++;
        }
        userCopy.jumlahkolom = user.jumlahkolom;
        if(found != 1){
            fwrite(&userCopy,sizeof(userCopy),1,fp1);
        }
        datake++;
	}
	fclose(fp);
	fclose(fp1);
	remove(table);
	rename("temp", table);
	return 0;
}
```
Fungsi `deleteTable` bertujuan untuk menghapus seluruh data dalam sebuah tabel. Pertama, kode membuka file dengan mode baca biner menggunakan fopen dan membuka file temp dengan mode tambah biner menggunakan fopen juga. Selanjutnya, menggunakan fread, kode membaca satu struktur user dari file yang berisi data tabel. Dilakukan perulangan untuk setiap user yang dibaca, dan pada setiap iterasi, sebuah struktur userCopy dibuat sebagai salinan dari user. Kemudian, userCopy ditulis ke file temp menggunakan fwrite. Setelah selesai perulangan, kedua file ditutup, file asli dihapus menggunakan remove, dan file temp diubah namanya menjadi nama file asli menggunakan rename. Fungsi deleteTable mengembalikan nilai 1.

Fungsi `deleteTableW` mirip dengan yang pertama, namun memiliki parameter tambahan yaitu index, kolom, dan where. index adalah indeks kolom yang akan digunakan untuk memeriksa kondisi (where). Jika kondisi terpenuhi, maka baris data tersebut tidak akan ditulis ke file temp. Jadi, deleteTableW digunakan untuk menghapus baris data tertentu berdasarkan kondisi dalam sebuah tabel. 

```c

int main() {
    int sockfd, ret;
    struct sockaddr_in serverAddr;
    int newSocket;
    struct sockaddr_in newAddr;
    socklen_t addr_size;
    char buffer[1024];
    pid_t childpid;

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        printf("[-]Error in connection.\n");
        exit(1);
    }
    printf("[+]Server Socket is created.\n");

    memset(&serverAddr, '\0', sizeof(serverAddr));
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(PORT);
    serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

    ret = bind(sockfd, (struct sockaddr *)&serverAddr, sizeof(serverAddr));
    if (ret < 0) {
        printf("Error in binding.\n");
        exit(1);
    }
    printf("[+]Bind to port %d\n", PORT);

    if (listen(sockfd, 10) == 0) {
        printf("Socket Listening....\n");
    } else {
        printf("Error in binding.\n");
    }
```
Selanjutnya adalah fungsi `main` yang berisi program untuk menjalankan command dari client. Potongan kode ini merupakan bagian awal dari program server, yang melakukan inisialisasi socket, binding ke alamat server, dan mengaktifkan socket untuk menerima koneksi dari client.

```c
 while (1) {
        newSocket = accept(sockfd, (struct sockaddr *)&newAddr, &addr_size);
        if (newSocket < 0) {
            exit(1);
        }
        printf("Connection accepted from %s:%d\n", inet_ntoa(newAddr.sin_addr), ntohs(newAddr.sin_port));

        if ((childpid = fork()) == 0) {
            close(sockfd);

            while (1) {
                recv(newSocket, buffer, 1024, 0);
                char *token;
                char buffercopy[32000];
                strcpy(buffercopy, buffer);
                char command[100][10000];
                token = strtok(buffercopy, ":");
                int i = 0;
                char database_used[1000];
                while (token != NULL) {
                    strcpy(command[i], token);
                    i++;
                    token = strtok(NULL, ":");
                }
                if (strcmp(command[0], "cUser") == 0) {
                     if (strcmp(command[3], "0") == 0) {
                        createUser(command[1], command[2]);
                        send(newSocket, "User created successfully.", strlen("User created successfully."), 0);
                    } else if (strcmp(command[3], "1") == 0) {
                        if (CheckUserExist(command[1]) == 1) {
                            send(newSocket, "User already exists.", strlen("User already exists."), 0);
                        } else {
                            createUser(command[1], command[2]);
                            send(newSocket, "User created successfully.", strlen("User created successfully."), 0);
                        }
                    }
...
```
Potongan kode di atas merupakan bagian dari loop utama dalam program server. Di dalam loop ini, server menggunakan `accept()` untuk menerima koneksi dari client dan membuat socket baru newSocket untuk komunikasi dengan client tersebut. Jika accept() gagal, program keluar dari loop.

Setelah berhasil menerima koneksi, server mencetak pesan bahwa koneksi telah diterima dari alamat IP dan port client yang terhubung. Selanjutnya, server melakukan `fork()` untuk membuat proses anak yang akan menangani komunikasi dengan client tersebut.

Di dalam proses anak, server menutup socket utama `sockfd` dan memulai loop untuk menerima data dari client menggunakan `recv()`. Data yang diterima disimpan dalam buffer buffer. Buffer tersebut kemudian diproses dengan membaginya menjadi token-token menggunakan `strtok()` dan menyimpannya dalam array `command`.

Selanjutnya, server melakukan pengecekan terhadap perintah yang diterima dari client. Jika perintah adalah `"cUser"`, maka server melakukan operasi sesuai dengan argumen yang diberikan. Misalnya, jika argumen ke-3 adalah "0", maka server akan langsung membuat pengguna baru dengan argumen ke-1 dan ke-2, dan mengirimkan respons ke client. Jika argumen ke-3 adalah "1", maka server akan memeriksa keberadaan pengguna sebelum membuat pengguna baru. Jika pengguna sudah ada, server mengirimkan respons bahwa pengguna tersebut sudah ada. Jika belum ada, server membuat pengguna baru dengan memanggil fungsi `createUser` dan mengirimkan respons sukses ke client.

```c
 }else if(strcmp(command[0], "cDatabase")==0){
					char lokasi[20000];
					snprintf(lokasi, sizeof lokasi, "databases/%s", command[1]);
					printf("loc = %s, database = %s\n", lokasi, command[1]);
					mkdir(lokasi,0777);
					forPermission(command[2], command[1]);
```
Jika perintah yang diterima adalah `"cDatabase"`, server akan membuat sebuah direktori dengan nama yang diberikan dalam argumen ke-1 dari perintah tersebut. Direktori tersebut akan ditempatkan di dalam folder "databases". Kode snprintf() digunakan untuk menggabungkan string "databases/" dengan nama direktori yang diberikan dalam argumen. Selanjutnya, server mencetak lokasi direktori yang akan dibuat, dan menggunakan fungsi mkdir() untuk membuat direktori tersebut dengan hak akses 0777 (yang memberikan hak akses penuh). Selain itu, server juga memanggil fungsi `forPermission()` untuk melakukan pengaturan izin akses pada direktori yang baru dibuat berdasarkan argumen ke-2 dari perintah.

```c
 }else if(strcmp(command[0], "uDatabase") == 0){
					if(strcmp(command[3], "0") != 0){
						int allowed = checkAllowedDatabase(command[2], command[1]);
						if(allowed != 1){
							char warn[] = "You cannot use database";
							send(newSocket, warn, strlen(warn), 0);
							bzero(buffer, sizeof(buffer));
						}else{
							strncpy(database_used, command[1], sizeof(command[1]));
							char warn[] = "You're using database now";
							printf("database = %s\n", database_used);
							send(newSocket, warn, strlen(warn), 0);
							bzero(buffer, sizeof(buffer));
						}
					}


                } else if (strcmp(command[0], "iPermission") == 0) {
                    if (CheckUserExist(command[1]) == 0) {
                        if (checkAllowedDatabase(command[1], command[2]) == 0) {
                            forPermission(command[1], command[2]);
                            printf("permission berhasil dibuat\n");
                        } else {
                            printf("permission sudah ada\n");
                        }
                    } else {
                        printf("user tidak ada\n");
                    }
...
```
Dalam potongan kode tersebut, kondisi else if pertama adalah untuk perintah `"uDatabase"`. Jika argumen ke-3 dari perintah tersebut bukan "0", maka server akan memeriksa apakah pengguna diizinkan menggunakan database yang diberikan dalam argumen ke-2. Jika pengguna tidak diizinkan, server akan mengirim pesan "You cannot use database" ke client. Jika pengguna diizinkan, server akan mengupdate variabel database_used dengan nama database yang diberikan dalam argumen ke-1, dan mengirim pesan "You're using database now" ke client.

Kondisi else if kedua adalah untuk perintah `"iPermission"`. Jika pengguna yang diberikan dalam argumen ke-1 belum ada, server akan memeriksa apakah izin akses untuk database yang diberikan dalam argumen ke-2 sudah ada atau belum. Jika belum ada, server akan memanggil fungsi `forPermission()` untuk membuat izin akses tersebut. Jika izin akses sudah ada, server akan mencetak pesan "permission sudah ada". Jika pengguna yang diberikan dalam argumen ke-1 tidak ada, server akan mencetak pesan "user tidak ada".

```c
 } else if(strcmp(command[0], "cTable")==0){
					printf("%s\n", command[1]);
					char *tokens;
					if(database_used[0] == '\0'){
						strcpy(database_used, "You're not selecting database yet");
						send(newSocket, database_used, strlen(database_used), 0);
						bzero(buffer, sizeof(buffer));
					}else{
                        char QueryList[100][10000];
                        char cpCommand[20000];
                        snprintf(cpCommand, sizeof cpCommand, "%s", command[1]);
                        tokens = strtok(cpCommand, "(), ");
                        int jumlah=0;
                        while( tokens != NULL ) {
                            strcpy(QueryList[jumlah], tokens);
                            printf("%s\n", QueryList[jumlah]);
                            jumlah++;
                            tokens = strtok(NULL, "(), ");
                        }
                        char buatTable[20000];
                        snprintf(buatTable, sizeof buatTable, "databases/%s/%s", database_used, QueryList[2]);
                        int iterasi = 0;
                        int iterasiData = 3;
                        struct table kolom;
                        while(jumlah > 3){
                            strcpy(kolom.data[iterasi], QueryList[iterasiData]);
                            printf("%s\n", kolom.data[iterasi]);
                            strcpy(kolom.type[iterasi], QueryList[iterasiData+1]);
                            iterasiData = iterasiData+2;
                            jumlah=jumlah-2;
                            iterasi++;
                        }
                        kolom.jumlahkolom = iterasi;
                        printf("iterasi = %d\n", iterasi);
                        FILE *fp;
                        printf("%s\n", buatTable);
                        fp=fopen(buatTable,"ab");
                        fwrite(&kolom,sizeof(kolom),1,fp);
                        fclose(fp);
                    }
```
Kondisi else if di atas adalah untuk perintah `"cTable"`. Server akan mencetak nama tabel yang diberikan dalam argumen ke-1. Jika belum ada database yang dipilih, server akan mengirim pesan "You're not selecting database yet" ke client. Jika sudah ada database yang dipilih, server akan melakukan parsing terhadap argumen ke-1 dan memperoleh informasi tentang nama tabel, kolom, dan tipe data. Server akan membuat file untuk tabel baru di direktori yang sesuai dengan database yang sedang dipilih, dan menulis struktur tabel yang terdiri dari nama kolom dan tipe data ke dalam file tersebut menggunakan `fwrite()`.

```c
}else if(strcmp(command[0], "dDatabase")==0){
					int allowed = checkAllowedDatabase(command[2], command[1]);
					if(allowed != 1){
						char warn[] = "You cannot access";
						send(newSocket, warn, strlen(warn), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}else{
						char hapus[20000];
						snprintf(hapus, sizeof hapus, "rm -r databases/%s", command[1]);
						system(hapus);
						char warn[] = "Database Has Been Deleted";
						send(newSocket, warn, strlen(warn), 0);
						bzero(buffer, sizeof(buffer));
					}

                
                }
                memset(buffer, 0, sizeof(buffer));
            }
        }
    }

    close(newSocket);

    return 0;
}
```
Kondisi else if ini untuk perintah `"dDatabase"`, server akan memeriksa apakah pengguna memiliki izin akses untuk database yang ingin dihapus. Jika tidak ada izin, server akan mengirim pesan "You cannot access" ke client dan melanjutkan ke iterasi berikutnya. Jika pengguna memiliki izin akses, server akan menghapus direktori database dengan menggunakan perintah rm -r melalui sistem operasi. Selanjutnya, server akan mengirim pesan "Database Has Been Deleted" ke client. Pada akhirnya, buffer akan direset dan proses akan kembali ke awal loop untuk menerima perintah selanjutnya dari client.

## Soal tidak bisa dikerjakan
1. Penyimpanan nama password user, permission ke user.txt dan permission.txt
2. DDL DML 
3. Realibility
4. Menjalankan program dengan ./database -u (user) -p (password)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <dirent.h>
#include <ctype.h>
#include <time.h>

#define PORT 4443

struct user_allowed {
    char name[10000];
    char password[10000];
};

int checkAllowed(char *username, char *password) {
    FILE *fp;
    struct user_allowed user;
    int found = 0;
    fp = fopen("home/grace/Documents/database/user.txt", "rb");
    if (fp == NULL) {
        printf("Cannot open user file.\n");
        return 0;
    }
    while (fread(&user, sizeof(user), 1, fp)) {
        if (strcmp(user.name, username) == 0) {
            if (strcmp(user.password, password) == 0) {
                found = 1;
                break;
            }
        }
    }
    fclose(fp);
    if (found == 0) {
        printf("No Permission\n");
        return 0;
    } else {
        return 1;
    }
}


int main(int argc, char *argv[]) {
    int user_allowed = 0;
    int id_user = geteuid();
    char database_used[1000];
    if (id_user == 0) {
        user_allowed = 1;
    } else {
        user_allowed = checkAllowed(argv[2], argv[4]);
    }
    if (user_allowed == 0) {
        return 0;
    }
    int clientSocket, ret;
    struct sockaddr_in serverAddr;
    char buffer[32000];

    clientSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (clientSocket < 0) {
        printf("[-]Error in connection.\n");
        exit(1);
    }
    printf("[+]Client Socket is created.\n");

    memset(&serverAddr, '\0', sizeof(serverAddr));
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(PORT);
    serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

    ret = connect(clientSocket, (struct sockaddr*)&serverAddr, sizeof(serverAddr));
    if (ret < 0) {
        printf("[-]Error in connection.\n");
        exit(1);
    }
    printf("[+]Connected to Server.\n");
    while (1) {
        printf("Client: \t");
        char input[1000];
        char copyinput[1000];
        char command[100][1000];
        char *token;
        int i = 0;
        scanf(" %[^\n]s", input);
        strcpy(copyinput, input);
        token = strtok(input, " ");
        while (token != NULL) {
            strcpy(command[i], token);
            i++;
            token = strtok(NULL, " ");
        }
        int wrongCommand = 0;
        if (strcmp(command[0], "CREATE") == 0) {
            if (strcmp(command[1], "USER") == 0 && strcmp(command[3], "IDENTIFIED") == 0 &&
                strcmp(command[4], "BY") == 0) {
                snprintf(buffer, sizeof(buffer), "cUser:%s:%s:%d", command[2], command[5], id_user);
                send(clientSocket, buffer, strlen(buffer), 0);
            } else if (strcmp(command[1], "DATABASE") == 0) {
                snprintf(buffer, sizeof(buffer), "cDatabase:%s:%s:%d", command[2], argv[2], id_user);
                send(clientSocket, buffer, strlen(buffer), 0);
            } else if (strcmp(command[1], "TABLE") == 0) {
                snprintf(buffer, sizeof(buffer), "cTable:%s", copyinput);
                send(clientSocket, buffer, strlen(buffer), 0);
            }
        } else if (strcmp(command[0], "GRANT") == 0 && strcmp(command[1], "PERMISSION") == 0 &&
                   strcmp(command[3], "INTO") == 0) {
            snprintf(buffer, sizeof(buffer), "iPermission:%s:%s:%d", command[2], command[4], id_user);
            send(clientSocket, buffer, strlen(buffer), 0);
        } else if (strcmp(command[0], "USE") == 0) {
            snprintf(buffer, sizeof(buffer), "uDatabase:%s:%s:%d", command[1], argv[2], id_user);
            send(clientSocket, buffer, strlen(buffer), 0);
        } else if (strcmp(command[0], "cekCurrentDatabase") == 0) {
            snprintf(buffer, sizeof(buffer), "%s", command[0]);
            send(clientSocket, buffer, strlen(buffer), 0);

        } else if (strcmp(command[0], "INSERT") == 0 && strcmp(command[1], "INTO") == 0) {
            snprintf(buffer, sizeof(buffer), "insert:%s", copyinput);
            send(clientSocket, buffer, strlen(buffer), 0);
        } else if (strcmp(command[0], "UPDATE") == 0) {
            snprintf(buffer, sizeof(buffer), "update:%s", copyinput);
            send(clientSocket, buffer, strlen(buffer), 0);
        } else if (strcmp(command[0], "DELETE") == 0) {
            snprintf(buffer, sizeof(buffer), "delete:%s", copyinput);
            send(clientSocket, buffer, strlen(buffer), 0);
        } else if (strcmp(command[0], "SELECT") == 0) {
            snprintf(buffer, sizeof(buffer), "select:%s", copyinput);
            send(clientSocket, buffer, strlen(buffer), 0);
        } else if (strcmp(command[0], "DROP") == 0) {
            if (strcmp(command[1], "DATABASE") == 0) {
                snprintf(buffer, sizeof(buffer), "dDatabase:%s:%s", command[2], argv[2]);
                send(clientSocket, buffer, strlen(buffer), 0);
            } else if (strcmp(command[1], "TABLE") == 0) {
                snprintf(buffer, sizeof(buffer), "dTable:%s:%s", command[2], argv[2]);
                send(clientSocket, buffer, strlen(buffer), 0);
            } else if (strcmp(command[1], "COLUMN") == 0) {
                snprintf(buffer, sizeof(buffer), "dColumn:%s:%s:%s", command[2], command[4], argv[2]);
                send(clientSocket, buffer, strlen(buffer), 0);
            }
        } 
     

        if (strcmp(command[0], ":exit") == 0) {
            send(clientSocket, command[0], strlen(command[0]), 0);
            close(clientSocket);
            printf("[-]Disconnected from server.\n");
            exit(1);
        }
        bzero(buffer, sizeof(buffer));
        if (recv(clientSocket, buffer, 1024, 0) < 0) {
            printf("[-]Error in receiving data.\n");
        } else {
            printf("Server: \t%s\n", buffer);
        }
    }

    return 0;
}

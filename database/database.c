#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>

#define PORT 4443

struct allowed{
	char name[10000];
	char password[10000];
};

struct used_database{
	char database[10000];
	char name[10000];
};

struct table{
	int jumlahkolom;
	char type[100][10000];
	char data[100][10000];
};

void createUser(const char *nama, const char *password){
	struct allowed user;
	strcpy(user.name, nama);
	strcpy(user.password, password);
	printf("%s %s\n", user.name, user.password);
	char fname[]={"home/grace/Documents/database/user.txt"};
	FILE *fp;
	fp=fopen(fname,"ab");
	fwrite(&user,sizeof(user),1,fp);
	fclose(fp);
}

int CheckUserExist(const char *username){
	FILE *fp;
	struct allowed user;
	int found=0;
	fp=fopen("home/grace/Documents/database/user.txt","rb");
	while(fread(&user,sizeof(user),1,fp) != 0){	
		if(strcmp(user.name, username)==0){
			fclose(fp);
			return 1;
		}
	}
	fclose(fp);
	return 0;
}

void forPermission(const char *nama, const char *database){
	struct used_database user;
	strcpy(user.name, nama);
	strcpy(user.database, database);
	printf("%s %s\n", user.name, user.database);
	char fname[]={"database/permission.txt"};
	FILE *fp;
	fp=fopen(fname,"ab");
	fwrite(&user,sizeof(user),1,fp);
	fclose(fp);
}

int checkAllowedDatabase(const char *nama, const char *database){
	FILE *fp;
	struct used_database user;
	fp=fopen("home/grace/Documents/database/permission.txt","rb");
	while(fread(&user,sizeof(user),1,fp) != 0){	
		if(strcmp(user.name, nama)==0 && strcmp(user.database, database)==0){
			fclose(fp);
			return 1;
		}
	}
	fclose(fp);
	return 0;
}

void writelog(const char *command, const char *nama){
	time_t rawtime;
	struct tm *timeinfo;
	time(&rawtime);
	timeinfo = localtime(&rawtime);
 
	char infoWriteLog[1000];

	FILE *file;
	file = fopen("logUser.log", "ab");

	sprintf(infoWriteLog, "%d-%.2d-%.2d %.2d:%.2d:%.2d::%s::%s\n",timeinfo->tm_year + 1900, timeinfo->tm_mon + 1, timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, nama, command);

	fputs(infoWriteLog, file);
	fclose(file);
	return;
}

int findCol(const char *table, const char *kolom){
	FILE *fp;
	struct table user;
	fp=fopen(table,"rb");	
	fread(&user,sizeof(user),1,fp);
	int index=-1;
	for(int i=0; i < user.jumlahkolom; i++){
		if(strcmp(user.data[i], kolom)==0){
			index = i;
		}
	}
	fclose(fp);
	return index;
}

int delCol(const char *table, int index){
	FILE *fp, *fp1;
	struct table user;
	fp=fopen(table,"rb");
	fp1=fopen("temp","wb");
	while(fread(&user,sizeof(user),1,fp) != 0){	
		struct table userCopy;
		int iterasi=0;
		for(int i=0; i< user.jumlahkolom; i++){
			if(i == index){
				continue;
			}
			strcpy(userCopy.data[iterasi], user.data[i]);
			strcpy(userCopy.type[iterasi], user.type[i]);
			iterasi++;
		}
		userCopy.jumlahkolom = user.jumlahkolom-1;
		fwrite(&userCopy,sizeof(userCopy),1,fp1);
	}
	fclose(fp);
	fclose(fp1);
	remove(table);
	rename("temp", table);
	return 0;
}


int updateCol(const char *table, int index, const char *ganti){
    FILE *fp, *fp1;
	struct table user;
	fp=fopen(table,"rb");
    fp1=fopen("temp","ab");
    int datake = 0;
	while(fread(&user,sizeof(user),1,fp) != 0){	
        struct table userCopy;
        int iterasi=0;
        for(int i=0; i< user.jumlahkolom; i++){
            if(i == index && datake!=0){
                strcpy(userCopy.data[iterasi], ganti);
            }else{
                strcpy(userCopy.data[iterasi], user.data[i]);
            }
            strcpy(userCopy.type[iterasi], user.type[i]);
            iterasi++;
        }
        userCopy.jumlahkolom = user.jumlahkolom;
        fwrite(&userCopy,sizeof(userCopy),1,fp1);
        datake++;
	}
	fclose(fp);
	fclose(fp1);
	remove(table);
	rename("temp", table);
	return 0;
}

int updateColumnW(const char *table, int index, const char *ganti, int indexGanti, const char *where){
    FILE *fp, *fp1;
	struct table user;
	fp=fopen(table,"rb");
    fp1=fopen("temp","ab");
    int datake = 0;
	while(fread(&user,sizeof(user),1,fp) != 0){	
        struct table userCopy;
        int iterasi=0;
        for(int i=0; i< user.jumlahkolom; i++){
            if(i == index && datake!=0 && strcmp(user.data[indexGanti], where)==0){
                strcpy(userCopy.data[iterasi], ganti);
            }else{
                strcpy(userCopy.data[iterasi], user.data[i]);
            }
            strcpy(userCopy.type[iterasi], user.type[i]);
            iterasi++;
        }
        userCopy.jumlahkolom = user.jumlahkolom;
        fwrite(&userCopy,sizeof(userCopy),1,fp1);
        datake++;
	}
	fclose(fp);
	fclose(fp1);
	remove(table);
	rename("temp", table);
	return 0;
}

int deleteTable(const char *table, const char *namaTable){
    FILE *fp, *fp1;
	struct table user;
	fp=fopen(table,"rb");
    fp1=fopen("temp","ab");	
	fread(&user,sizeof(user),1,fp);
    struct table userCopy;
	for(int i=0; i < user.jumlahkolom; i++){
        strcpy(userCopy.data[i], user.data[i]);
        strcpy(userCopy.type[i], user.type[i]);
	}
    userCopy.jumlahkolom = user.jumlahkolom;
    fwrite(&userCopy,sizeof(userCopy),1,fp1);
    fclose(fp);
	fclose(fp1);
    remove(table);
	rename("temp", table);
	return 1;
}

int deleteTableW(const char *table, int index, const char *kolom, const char *where){
    FILE *fp, *fp1;
	struct table user;
	fp=fopen(table,"rb");
    fp1=fopen("temp","ab");
    int datake = 0;
	while(1){	
        int found = 0;
		fread(&user,sizeof(user),1,fp);
        if(feof(fp)){
			break;
		}
        struct table userCopy;
        int iterasi=0;
        for(int i=0; i< user.jumlahkolom; i++){
            if(i == index && datake!=0 && strcmp(user.data[i], where)==0){
                found = 1;
            }
            strcpy(userCopy.data[iterasi], user.data[i]);
            strcpy(userCopy.type[iterasi], user.type[i]);
            iterasi++;
        }
        userCopy.jumlahkolom = user.jumlahkolom;
        if(found != 1){
            fwrite(&userCopy,sizeof(userCopy),1,fp1);
        }
        datake++;
	}
	fclose(fp);
	fclose(fp1);
	remove(table);
	rename("temp", table);
	return 0;
}



int main() {
    int sockfd, ret;
    struct sockaddr_in serverAddr;
    int newSocket;
    struct sockaddr_in newAddr;
    socklen_t addr_size;
    char buffer[1024];
    pid_t childpid;

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        printf("[-]Error in connection.\n");
        exit(1);
    }
    printf("[+]Server Socket is created.\n");

    memset(&serverAddr, '\0', sizeof(serverAddr));
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(PORT);
    serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

    ret = bind(sockfd, (struct sockaddr *)&serverAddr, sizeof(serverAddr));
    if (ret < 0) {
        printf("Error in binding.\n");
        exit(1);
    }
    printf("[+]Bind to port %d\n", PORT);

    if (listen(sockfd, 10) == 0) {
        printf("Socket Listening....\n");
    } else {
        printf("Error in binding.\n");
    }

    while (1) {
        newSocket = accept(sockfd, (struct sockaddr *)&newAddr, &addr_size);
        if (newSocket < 0) {
            exit(1);
        }
        printf("Connection accepted from %s:%d\n", inet_ntoa(newAddr.sin_addr), ntohs(newAddr.sin_port));

        if ((childpid = fork()) == 0) {
            close(sockfd);

            while (1) {
                recv(newSocket, buffer, 1024, 0);
                char *token;
                char buffercopy[32000];
                strcpy(buffercopy, buffer);
                char command[100][10000];
                token = strtok(buffercopy, ":");
                int i = 0;
                char database_used[1000];
                while (token != NULL) {
                    strcpy(command[i], token);
                    i++;
                    token = strtok(NULL, ":");
                }
                if (strcmp(command[0], "cUser") == 0) {
                     if (strcmp(command[3], "0") == 0) {
                        createUser(command[1], command[2]);
                        send(newSocket, "User created successfully.", strlen("User created successfully."), 0);
                    } else if (strcmp(command[3], "1") == 0) {
                        if (CheckUserExist(command[1]) == 1) {
                            send(newSocket, "User already exists.", strlen("User already exists."), 0);
                        } else {
                            createUser(command[1], command[2]);
                            send(newSocket, "User created successfully.", strlen("User created successfully."), 0);
                        }
                    }
                }else if(strcmp(command[0], "cDatabase")==0){
					char lokasi[20000];
					snprintf(lokasi, sizeof lokasi, "databases/%s", command[1]);
					printf("loc = %s, database = %s\n", lokasi, command[1]);
					mkdir(lokasi,0777);
					forPermission(command[2], command[1]);

                }else if(strcmp(command[0], "uDatabase") == 0){
					if(strcmp(command[3], "0") != 0){
						int allowed = checkAllowedDatabase(command[2], command[1]);
						if(allowed != 1){
							char warn[] = "You cannot use database";
							send(newSocket, warn, strlen(warn), 0);
							bzero(buffer, sizeof(buffer));
						}else{
							strncpy(database_used, command[1], sizeof(command[1]));
							char warn[] = "You're using database now";
							printf("database = %s\n", database_used);
							send(newSocket, warn, strlen(warn), 0);
							bzero(buffer, sizeof(buffer));
						}
					}


                } else if (strcmp(command[0], "iPermission") == 0) {
                    if (CheckUserExist(command[1]) == 0) {
                        if (checkAllowedDatabase(command[1], command[2]) == 0) {
                            forPermission(command[1], command[2]);
                            printf("permission berhasil dibuat\n");
                        } else {
                            printf("permission sudah ada\n");
                        }
                    } else {
                        printf("user tidak ada\n");
                    }

                } else if(strcmp(command[0], "cTable")==0){
					printf("%s\n", command[1]);
					char *tokens;
					if(database_used[0] == '\0'){
						strcpy(database_used, "You're not selecting database yet");
						send(newSocket, database_used, strlen(database_used), 0);
						bzero(buffer, sizeof(buffer));
					}else{
                        char QueryList[100][10000];
                        char cpCommand[20000];
                        snprintf(cpCommand, sizeof cpCommand, "%s", command[1]);
                        tokens = strtok(cpCommand, "(), ");
                        int jumlah=0;
                        while( tokens != NULL ) {
                            strcpy(QueryList[jumlah], tokens);
                            printf("%s\n", QueryList[jumlah]);
                            jumlah++;
                            tokens = strtok(NULL, "(), ");
                        }
                        char buatTable[20000];
                        snprintf(buatTable, sizeof buatTable, "databases/%s/%s", database_used, QueryList[2]);
                        int iterasi = 0;
                        int iterasiData = 3;
                        struct table kolom;
                        while(jumlah > 3){
                            strcpy(kolom.data[iterasi], QueryList[iterasiData]);
                            printf("%s\n", kolom.data[iterasi]);
                            strcpy(kolom.type[iterasi], QueryList[iterasiData+1]);
                            iterasiData = iterasiData+2;
                            jumlah=jumlah-2;
                            iterasi++;
                        }
                        kolom.jumlahkolom = iterasi;
                        printf("iterasi = %d\n", iterasi);
                        FILE *fp;
                        printf("%s\n", buatTable);
                        fp=fopen(buatTable,"ab");
                        fwrite(&kolom,sizeof(kolom),1,fp);
                        fclose(fp);
                    }
                    }else if(strcmp(command[0], "dDatabase")==0){
					int allowed = checkAllowedDatabase(command[2], command[1]);
					if(allowed != 1){
						char warn[] = "You cannot access";
						send(newSocket, warn, strlen(warn), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}else{
						char hapus[20000];
						snprintf(hapus, sizeof hapus, "rm -r databases/%s", command[1]);
						system(hapus);
						char warn[] = "Database Has Been Deleted";
						send(newSocket, warn, strlen(warn), 0);
						bzero(buffer, sizeof(buffer));
					}

                
                }
                memset(buffer, 0, sizeof(buffer));
            }
        }
    }

    close(newSocket);

    return 0;
}